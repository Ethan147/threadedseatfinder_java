package assignment6;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;

///////////////////////////
/*
 * Class interfaces with ThreadedTicketClient to 
 * create threads and defines customer characteristics
 */
///////////////////////////
public class TicketClient {
	ThreadedTicketClient tc;
	String result     = "dummy";
	String hostName   = "";
	String threadName = "";

	int clientNumber;
	
	// Constructor with hostname and threadname definitions
	//////////////////////////////////////////////////
	TicketClient(String hostname, String threadname) {
		tc         = new ThreadedTicketClient(this, hostname, threadname);
		hostName   = hostname;
		threadName = threadname;
	}

	// constructor defaulted as localhost
	///////////////////////////
	TicketClient(String name) {
		this("localhost", name);
	}

	// constructo with no specifications
	////////////////
	TicketClient() {
		this("localhost", "unnamed client");
	}

	// request function which generates a random "line" at the ticket booth 
	// at a length of 100-1000, continually generating more customers as needed
	///////////////////////////////////
	void requestTicketRandomAtTicketOffice() {

		Random rand         = new Random(); 
		int customersInLine;
		
		while(true) {
		
			customersInLine = rand.nextInt(900) + 100;
			
			for( int x = 0; x < customersInLine; x++ ) {
				
				this.clientNumber = x + 1;
				tc.setClientNumber(clientNumber);
				tc.run();
			}
		}
	}
	
	// request a single ticket for client 1 of this ticket booth
	// should not be used in concurrence with requestTicketRandomAtTicketOffice()
	//////////////////////////////////
	void requestSingleClientTicket() {
		
		this.clientNumber = 1;
		tc.setClientNumber(1);
		tc.run();
	}

	// unused sleep function for thread
	//////////////
	void sleep() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}