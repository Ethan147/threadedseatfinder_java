package assignment6;

////////////
/*
 * Class Seat serves as an easy returnable class for methods.
 * Seat contains all necessary identifiers for a seat in the Bates Recital Hall.
 * Seat also contains row letter-to-number and number-to-letter translation methods.
 */
////////////
class Seat {
	
	String seatColumn;
	String seatRow;
	int    seatNumber;
	
	///////////////////////////////////////////////////////////
	Seat( String seatColumn, String seatRow, int seatNumber ) {
		
		this.seatColumn = seatColumn;
		this.seatRow    = seatRow;
		this.seatNumber = seatNumber;
	}
	
	////////////////////////
	String getSeatColumn() {
		return this.seatColumn;
	}
	
	/////////////////////
	String getSeatRow() {
		return this.seatRow;
	}
	
	/////////////////////
	int getSeatNumber() {
		return this.seatNumber;
	}
	
	////////////////////////////////////////////////
	synchronized static int translateRowLetter( String letter ) {
		
		switch( letter.toUpperCase() ) {
		case "A":  return 0;
		case "B":  return 1;
		case "C":  return 2;
		case "D":  return 3;
		case "E":  return 4;
		case "F":  return 5;
		case "G":  return 6;
		case "H":  return 7;
		case "J":  return 8;
		case "K":  return 9;
		case "L":  return 10;
		case "M":  return 11;
		case "N":  return 12;
		case "P":  return 13;
		case "Q":  return 14;
		case "R":  return 15;
		case "S":  return 16;
		case "T":  return 17;
		case "U":  return 18;
		case "V":  return 19;
		case "W":  return 20;
		case "X":  return 21;
		case "Y":  return 22;
		case "Z":  return 23;
		case "AB": return 24;
		default:   return -1;
		}
	}
	
	/////////////////////////////////////////////////////////////
	synchronized static String translateRowNumber( int number ) {
		
		switch( number ) {
		case 0:  return "A";
		case 1:  return "B";
		case 2:  return "C";
		case 3:  return "D";
		case 4:  return "E";
		case 5:  return "F";
		case 6:  return "G";
		case 7:  return "H";
		case 8:  return "J";
		case 9:  return "K";
		case 10: return "L";
		case 11: return "M";
		case 12: return "N";
		case 13: return "P";
		case 14: return "Q";
		case 15: return "R";
		case 16: return "S";
		case 17: return "T";
		case 18: return "U";
		case 19: return "V";
		case 20: return "W";
		case 21: return "X";
		case 22: return "Y";
		case 23: return "Z";
		case 24: return "Ab";
		default: return "error";
		}
	}
}