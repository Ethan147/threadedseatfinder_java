package assignment6;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;


////////////////////////////////////////////////
/*
 * ThreadedTicketClient class acts as the driver of the 
 * ticket booths (client threads). 
 * 
 * Handles requests to server and receiving notification
 * from server.
 */
////////////////////////////////////////////////
class ThreadedTicketClient implements Runnable {
	String       hostname   = "127.0.0.1";
	String       threadname = "X";
	TicketClient sc;
	int          clientNumber;
	
	// constructor to record basic customer characteristics
	//////////////////////////////////////////////////////////////////////////////////
	public ThreadedTicketClient(TicketClient sc, String hostname, String threadname) {
		this.sc         = sc;
		this.hostname   = hostname;
		this.threadname = threadname;
	}
	
	// method to set client number for thread
	////////////////////////////////////////
	public void setClientNumber( int num ) {
		
		this.clientNumber = num;
	}

	// alternate unused wait-for-server function
	////////////////////////////////////
	synchronized boolean threadWait( ) {
		
		int attempts = 0;
		attempts++;
		ClientConnectionManager.processServerMessage();
		
		if(ClientConnectionManager.serverMessageContains(threadname)) {

				System.out.println(threadname + "properly exiting");
				return true;
		}
		else if( attempts == 40 ) {
			System.out.println("IMPROPER EXIT for " + threadname);
			return true;
		}
		else {
			
			try {
				System.out.println(threadname + " sleeping");
				Thread.sleep(1);
			}
			catch(InterruptedException e) {
				
				System.out.println("sleeping error");
			}
		}
		
		return false;
	}
	
	// basic run, connects to server and then waits for a confirmation
	///////////////////
	public void run() {
		System.out.flush();
		
		try {

			ClientConnectionManager.synchronizedStartup( this.hostname, threadname, this.clientNumber );
			
			int attempts = 0;
			while(true) {

				attempts++;
				ClientConnectionManager.processServerMessage();

				if(ClientConnectionManager.serverMessageContains(threadname)) {

						return;
				}
				else if( attempts == 40 ) {

					return;
				}
				else {
					
					try {

						Thread.sleep(1);
					}
					catch(InterruptedException e) {

					}
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
}