package assignment6;

import static org.junit.Assert.fail;

import org.junit.Test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import org.junit.Test;
import java.util.concurrent.*;


///////////////////////////////
/*
 * JUnit is the main method of interacting with this project.
 * The various tests create client-threads that we could as "booths".
 * 
 * We assume that no more than three booths are created at once.
 */
///////////////////////////////
public class TestTicketOffice {

	
	// Just test if server will connect and see if we can request with a ticket booth
	@Test /////////////////////////
	public void basicServerTest() {
		System.out.println("\n\n\n ********** basicServerTest() **********");
		
		try {
			TicketServer.start(16789);
		} catch (Exception e) {
			fail();
		}
		TicketClient client = new TicketClient();
		client.requestSingleClientTicket();
	}

	// test to see if we can request with two ticket booths
	@Test //////////////////////////////////////
	public void testServerCachedHardInstance() {
		System.out.println("\n\n\n ********** testServerCachedHardInstance() **********");
		
		try {
			TicketServer.start(16790);
		} catch (Exception e) {
			fail();
		}
		TicketClient client1 = new TicketClient("localhost", "c1");
		TicketClient client2 = new TicketClient("localhost", "c2");
		client1.requestSingleClientTicket();
		client2.requestSingleClientTicket();
	}

	// test to see if we can request with three simultaneous ticket booths
	@Test ////////////////////////////////////
	public void twoNonConcurrentServerTest() {
		System.out.println("\n\n\n ********** twoNonConcurrentServerTest() **********");
		
		try {
			TicketServer.start(16791);
		} catch (Exception e) {
			fail();
		}
		TicketClient c1 = new TicketClient("nonconc1");
		TicketClient c2 = new TicketClient("nonconc2");
		TicketClient c3 = new TicketClient("nonconc3");
		c1.requestSingleClientTicket();
		c2.requestSingleClientTicket();
		c3.requestSingleClientTicket();
	}

	// test to see if we can request with three concurrent ticket booths
	@Test ///////////////////////////////////
	public void threeConcurrentServerTest() {
		System.out.println("\n\n\n ********** threeConcurrentServerTest() **********");
		
		try {
			TicketServer.start(16792);
		} catch (Exception e) {
			fail();
		}
		final TicketClient c1 = new TicketClient("conc1");
		final TicketClient c2 = new TicketClient("conc2");
		final TicketClient c3 = new TicketClient("conc3");
		
		Thread t1 = new Thread() {
			public void run() {
				c1.requestSingleClientTicket();
			}
		};
		Thread t2 = new Thread() {
			public void run() {
				c2.requestSingleClientTicket();
			}
		};
		Thread t3 = new Thread() {
			public void run() {
				c3.requestSingleClientTicket();
			}
		};
		
		t1.start();
		t2.start();
		t3.start();
		try {
			t1.join();
			t2.join();
			t3.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// tests the programs handling of two randomize ticket booth "lines"
	@Test /////////////////////////////////////
	public void randomCustomersAtBoothsTest() {
		System.out.println("\n\n\n ********** randomCustomersAtBoothsTes() **********");
		
		try {
			TicketServer.start(16792);
		} catch (Exception e) {
			fail();
		}
		final TicketClient c1 = new TicketClient("conc1");
		final TicketClient c2 = new TicketClient("conc2");
		
		Thread t1 = new Thread() {
			public void run() {
				c1.requestTicketRandomAtTicketOffice();
			}
		};
		Thread t2 = new Thread() {
			public void run() {
				c2.requestTicketRandomAtTicketOffice();
			}
		};
		
		t1.start();
		t2.start();
		try {
			t1.join();
			t2.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
