package assignment6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

////////////////////////////////////////////////
/*
 * ThreadedTicketServer listens to and responds to
 * client (booth) ticket requests.
 */
////////////////////////////////////////////////
class ThreadedTicketServer implements Runnable {

	String hostname   = "127.0.0.1";
	String threadname = "X";
	String testcase;
	TicketClient sc;
	
	// opens the server socket and then continually accepts new connections to 
	// the socket, allocating a thread for each
	///////////////////
	public void run() {
	
		ServerSocket serverSocket;
		try {
	
			serverSocket        = new ServerSocket (TicketServer.PORT);
			
			while(true) {
				
				Socket clientSocket = serverSocket.accept();

				Runnable connectionHandler = new ServerConnectionManager(clientSocket);
				new Thread(connectionHandler).start();
			}
						
		} catch (IOException e) {

			e.printStackTrace();
		}

	}
}