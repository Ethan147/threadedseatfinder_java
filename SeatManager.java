package assignment6;


///////////////////
/*
 * SeatManager handles finding the best available seat,
 * reserving that seat, and printing a seat ticket.
 * 
 * SeatManager allows a user to clear all seat reservations.
 * SeatManager functions mostly return Seat classes.
 */
///////////////////
class SeatManager {
	
	static boolean Hall_Full = false;
	
	static boolean Seat_Taken              = true;
	static boolean Seat_Available          = false;
	static int     Seat_Offset_House_Left  = 101;
	static int     Seat_Offset_Middle      = 101 + 7;
	static int     Seat_Offset_House_Right = 101 + 7 + 14;
	
	// A - X vertical (missing I & O), 108-121 horizontal
	static boolean middleSeats[][]     = new boolean[22][14]; 
	static boolean houseRightSeats[][] = new boolean[25][7];
	static boolean houseLeftSeats[][]  = new boolean[23][7];

	// for clearing all seat reservations
	/////////////////////////////////////////////////////
	synchronized static void clearAllSeatReservations() {
		
		for( int y = 0; y < 22; y++ ) {
			for( int x = 0; x < 14; x++ ) {
				middleSeats[y][x] = Seat_Available;
			}
		}
		
		for( int y = 0; y < 25; y++ ) {
			for( int x = 0; x < 7; x++ ) {
				houseRightSeats[y][x] = Seat_Available;
			}
		}
		
		for( int y = 0; y < 23; y++ ) {
			for( int x = 0; x < 7; x++ ) {
				houseLeftSeats[y][x] = Seat_Available;
			}
		}
	}
	
	// synchronized finding and reservation of a seat, prints the ticket if successful
	/////////////////////////////////////////////////////////////////////////////////////
	synchronized static void findAndReserve( String clientBoothName, int clientNumber ) {
		
		Seat toReserve = SeatManager.bestAvailableSeat();
		
		if( toReserve.getSeatNumber() == -1 ) {
			
			System.out.println("sorry we are sold out");
			System.exit(0);
		}
		
		SeatManager.printTicketSeat(toReserve, clientBoothName, clientNumber);
		
		ServerConnectionManager.sendClientMessage( "-" + clientBoothName );
	}
	
	// fins the client the best available seat in the house
	// best is defined as closest to the center stage
	//////////////////////////////////////////////
	synchronized static Seat bestAvailableSeat() {
		
		if( Hall_Full == true ) {
			
			// return seat number of -1;
			return (new Seat( "", "", -1 ));
		}
 		
		Seat reserveSeat;
		
		for( int row = 0; row < 25; row++ ) {

			if( row < 22 ) {
				
				reserveSeat = reserveMiddleSeatInRow( row );
				if( reserveSeat.getSeatNumber() != -1 ) {
					
					return reserveSeat;
				}
			}
			
			reserveSeat = reserveHouseRightSeatInRow( row );
			if( reserveSeat.getSeatNumber() != -1 ) {
				
				return reserveSeat;
			}
			
			if( row > 1 ) {
				reserveSeat = reserveHouseLeftSeatInRow( row );
				if( reserveSeat.getSeatNumber() != -1 ) {
					
					return reserveSeat;
				}
			}
		}
		
		// no seat found
		Hall_Full = true; 
		return (new Seat( "", "", -1 ));		
	}

	// helper function to find best available seat in the middle
	/////////////////////////////////////////////////////
    static Seat reserveMiddleSeatInRow( int middleRow ) {
		
		// check middle:
		for( int x = 0; x < middleSeats[0].length; x++ ) {
		
			// find closest to middle of Row
			int seatCheck;
			
			switch( x ) {
			case 0:  seatCheck = 6;  break;
			case 1:  seatCheck = 7;  break;
			case 2:  seatCheck = 8;  break;
			case 3:  seatCheck = 5;  break;
			case 4:  seatCheck = 9;  break;
			case 5:  seatCheck = 4;  break;
			case 6:  seatCheck = 10; break;
			case 7:  seatCheck = 3;  break;
			case 8:  seatCheck = 11; break;
			case 9:  seatCheck = 2;  break;
			case 10: seatCheck = 12; break;
			case 11: seatCheck = 1;  break;
			case 12: seatCheck = 13; break;
			case 13: seatCheck = 0;  break;
			default: seatCheck = -1; break;
			}
			
			if( middleSeats[ middleRow ][ seatCheck ] == Seat_Available ) {
				
				middleSeats[ middleRow ][ seatCheck ] = Seat_Taken;
				
				int    seatNumber   = seatCheck + Seat_Offset_Middle;
				return (new Seat( "Middle", Seat.translateRowNumber(middleRow), seatNumber ));
			}
		}
		
		// no seat found
		return (new Seat( "", "", -1));
    }

    // helper function to reserve best available seat in the right
	////////////////////////////////////////////////////////
    static Seat reserveHouseRightSeatInRow( int rightRow ) {
    	
    	if( (rightRow < 0)  || (rightRow > 24) ) { 
    		return (new Seat( "", "", -1)); 
    	}

    	if( rightRow == 24 ) {
			
			if( houseRightSeats[ rightRow ][0] == Seat_Available ) {	
				houseRightSeats[ rightRow ][0] = Seat_Taken;
				return (new Seat( "House-Right", Seat.translateRowNumber(rightRow), 127));
			}
			
			else if( houseRightSeats[ rightRow ][1] == Seat_Available ) {
				houseRightSeats[ rightRow ][1] = Seat_Taken;
				return (new Seat( "House-Right", Seat.translateRowNumber(rightRow), 128));
			}
			
			else return (new Seat( "", "", -1));
		}
    	
		for( int x = 0; x < houseRightSeats[0].length; x++ ) {
			
			if( houseRightSeats[ rightRow ][ x ] == Seat_Available ) {
				
				houseRightSeats[ rightRow ][ x ] = Seat_Taken;
				
				int    seatNumber   = x + Seat_Offset_House_Right;
				return (new Seat( "House-Right", Seat.translateRowNumber(rightRow), seatNumber ));
			}
		}
		
		// no seat found
		return (new Seat( "", "", -1));
    }
    
    // helper function to reserve best available seat in the left
	//////////////////////////////////////////////////////
    static Seat reserveHouseLeftSeatInRow( int leftRow ) {
		
    	if( (leftRow < 2) || (leftRow > 24) ) { 
    		return (new Seat( "", "", -1)); 
    	}
    	
    	if( leftRow == 2 ) {
    		for( int x = houseLeftSeats[0].length - 2; x >= 0; x-- ) {
    			
    			if( houseLeftSeats[ leftRow - 2 ][ x ] == Seat_Available ) {
    				houseLeftSeats[ leftRow - 2 ][ x ] = Seat_Taken;
    			
    				int seatNumber = x + Seat_Offset_House_Left;
    				return (new Seat( "House-Left", Seat.translateRowNumber( 2 ), seatNumber ));
    			}
    		}
    		
    		// no seat found in row
    		return (new Seat( "", "", -1 ));
    	}
    	
    	if( leftRow == 24 ) {
    		for( int x = houseLeftSeats[0].length - 1; x >= 0; x-- ) {
    			
    			if( houseLeftSeats[ leftRow - 2 ][ x ] == Seat_Available ) {
    				houseLeftSeats[ leftRow - 2 ][ x ] = Seat_Taken;
    				
    				int seatNumber;
    				
    				switch(x) {
    				case 0:  seatNumber = 101; break;
    				case 1:  seatNumber = 102; break;
    				case 2:  seatNumber = 103; break;
    				case 3:  seatNumber = 104; break;
    				case 4:  seatNumber = 116; break;
    				case 5:  seatNumber = 117; break;
    				case 6:  seatNumber = 118; break;
    				default: seatNumber = -1;  break;
    				}

    				return (new Seat( "House-Left", Seat.translateRowNumber( leftRow ), seatNumber ));
    			}
    		}
    		
    		// no seat found in row
    		return (new Seat( "", "", -1 ));
    	}
    	
		for( int x = houseLeftSeats[0].length - 1; x >= 0; x-- ) {
			
			if( houseLeftSeats[ leftRow - 2 ][ x ] == Seat_Available ) {
				
				houseLeftSeats[ leftRow - 2 ][ x ] = Seat_Taken;
				
				int    seatNumber   = x + Seat_Offset_House_Left;
				return (new Seat( "House-Left", Seat.translateRowNumber( leftRow ), seatNumber ));
			}
		}
		
		// no seat found
		return (new Seat( "", "", -1 ));
    }
		
    // prints a formatted ticket which specifies seat number, boothName, clientNumber, row, and section
	///////////////////////////////////////////////////////////////////////////////////////////////////
	synchronized static void printTicketSeat( Seat reservedSeat, String boothName, int clientNumber ) {
		
		int seatNumber = reservedSeat.seatNumber;
		String row = reservedSeat.getSeatRow();
		String seatRow;
		
		int rowNum;
		
		if( row.toUpperCase().equals("AB") ) {
			seatRow = "A";
			rowNum  =  25;
		}
		else {
			seatRow = row;

			// add 1 to get non-array referencing row number
			rowNum     = Seat.translateRowLetter(seatRow) + 1;
		}

		String seatCol = reservedSeat.getSeatColumn();
		
		System.out.println("Ticket: Seat " + seatNumber + " of row " + rowNum + "(" + seatRow + ") in " 
							+ seatCol + " section purchased and reserved for ticket office " + boothName + "'s client number " + clientNumber + ".");
	}	
}