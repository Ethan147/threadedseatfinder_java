package assignment6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

///////////////////////////
/*
 * Class that simply initializes ThreadedTicketServer.
 */
///////////////////////////
public class TicketServer {
	static int PORT = 2222;
	
	final static int MAXPARALLELTHREADS = 3;
	
	// basic constructor to create a server thread
	/////////////////////////////////////////////////////////////
	public static void start(int portNumber) throws IOException {
		
		PORT                  = portNumber;
		Runnable serverThread = new ThreadedTicketServer();
		Thread t              = new Thread(serverThread);
		t.start();
	}
}

