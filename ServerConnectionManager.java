package assignment6;

import java.io.*;
import java.net.*;

///////////////////////////////////////////////////
/*
 * Helper class for server, helps synchronize many 
 * processes which otherwise create race conditions
 * by allowing itself to be opened as a thread.
 */
///////////////////////////////////////////////////
class ServerConnectionManager implements Runnable {
	
	static Socket   socketCon = null;
	static PrintWriter    out = null;  
	static BufferedReader in  = null;   
	
	// creates the server side connection of a socket
	////////////////////////////////////////////
	ServerConnectionManager( Socket inSocket ) {
		
		try {
			socketCon = inSocket;
			out       = new PrintWriter     (socketCon.getOutputStream(), true);
			in        = new BufferedReader  (new InputStreamReader(socketCon.getInputStream()));
		}
		catch( IOException e ) {
			
			System.out.println("error on ServerConnectionManager constructor");
		}
	}
	
	// synchronize sending of a message to client
	//////////////////////////////////////////////////////////////
	synchronized static void sendClientMessage( String message ) {
		
		out.println(message);
	}
	
	// takes care of client specific server fucntions,
	// returns message when server is through.
	////////////////////
	public void run( ) {
		
		String[] clientMessage;
		String   clientBoothName = null;
		
		try {
			while( this.in.ready() ) { 
				
				try {
					clientMessage = this.in.readLine().split(" ");
				} catch (NullPointerException e) {
					return;
				}
				
				if( clientMessage[0].equals("getseat")) {
				
					int clientNumber = 1;
					
					clientBoothName  = clientMessage[1];
					
					if( clientMessage.length == 3 ) {
					
						clientNumber = Integer.parseInt( clientMessage[2] );
					}
						
					SeatManager.findAndReserve( clientBoothName, clientNumber );
				}
			}
		}
		catch( IOException e ) {
			
			System.out.println("improper exit of ServerConnectionManager run()");
		}
	}
	
}