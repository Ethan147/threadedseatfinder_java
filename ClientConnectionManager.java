package assignment6;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

///////////////////////////////
/*
 * ClientConnectionManager abstacts and simplifies 
 * the synchronized tasks of connecting to the server from different thread.
 * 
 * Prevents many race conditions.
 */
///////////////////////////////
class ClientConnectionManager {
	
	static boolean ServerinUse = false;
	static int     ServerPORT  = -1;
	
	static Socket         serverSocket;
	static Socket 		  clientSocket;
	static PrintWriter    out;
	static BufferedReader in;
	static String         hostname;
	static boolean        serverInitialize = false;
	static ArrayList<String> serverReturnMessage = new ArrayList<String>();
	
	// optional, unused "lock" method
	////////////////////////////////////////////////
	static void setServerInUse( boolean useState ) {
		
		ServerinUse = useState;
	}
	
	// optional, unused server initialization setter
	//////////////////////////////////////////////////////////
	static void setServerInitialization( boolean initState ) {
		
		serverInitialize = initState;
	}
	
	// optional, unused server initialization getter
	///////////////////////////////////////////
	static boolean getServerInitialization( ) {
		
		return serverInitialize;
	}
	
	// optional unused lock getter
	/////////////////////////////////////
	static boolean getServerUseState( ) {
		
		return ServerinUse;
	}
	
	// defines the local server port
	///////////////////////////////////////////
	static void defineServerPorts( int PORT ) {
		
		ServerPORT = PORT;
	}
	
	// defines the server host name
	///////////////////////////////////////////////
	static void defineHostName( String hostName ) {
		
		hostname = hostName;
	}
	
	// connects to server for a client thread (booth)
	/////////////////////////////////////////////
	synchronized static void connectToServer( ) {
		
		String printMessage = null;

		try {
			
			serverSocket = new Socket (hostname, ServerPORT);
			out          = new PrintWriter (serverSocket.getOutputStream(), true);
			in		      = new BufferedReader (new InputStreamReader(serverSocket.getInputStream()));
		} catch(IOException e) {
			
			printMessage = "error";
		}
			
		if( printMessage != null ) {
			System.out.println("connectToServer " + printMessage);
		}
	}

	// unused option close-server-connection function
	//////////////////////////////////////
	static void closeServerConnection( ) {
		
		String printMessage = null;
		
		try { 
			
			serverSocket.close(); 
			out.close();
		} catch(IOException e) { 
			
			printMessage = "error";  
		}
		
		if( printMessage != null ) {
			System.out.println("closeServerConnection " + printMessage );
		}
	}
	
	// synchronized sending of server messages, prevents race conditions
	//////////////////////////////////////////////////////////////
	synchronized static void sendServerMessage( String message ) {
		
		out.println(message); 
		out.flush();
	}
	
	// processes a server message, records server message in data structure
	//////////////////////////////////////////////////
	synchronized static void processServerMessage( ) {
		
		try { 
			
			String[] serverMessages = null;
			String   serverMessage  = null;

			if( in.ready() ) {
				serverMessages = in.readLine().split("-");
			
				for( int x = 0; x < serverMessages.length; x++ ) {
					
					serverMessage = serverMessages[x];
					serverReturnMessage.add(serverMessage.trim());
				}
			}
		} catch(IOException e) { 
			
			System.out.println("processServerMessage error");
		}
	}
	
	// thread startup method, synchronous implementation prevents race conditions
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	synchronized static void synchronizedStartup( String hostname, String threadname, int clientNumber ) {

		ClientConnectionManager.defineHostName    ( hostname     );
		ClientConnectionManager.defineServerPorts ( TicketServer.PORT );
		
		ClientConnectionManager.connectToServer();
		ClientConnectionManager.sendServerMessage("getseat " + threadname + " " + clientNumber);
	}
	
	// checks if server has send a input string
	///////////////////////////////////////////////////////////////////
	 static boolean serverMessageContains( String input ) {
		
		if( serverReturnMessage.contains(input) ) {
			serverReturnMessage.remove(input);
			return true;
		}
		
		return false;
	}
	
	// options unused chekc if server is ready
	///////////////////////////////////////////
	synchronized static boolean inputReady( ) {
		
		if( serverReturnMessage.isEmpty() ) {
			return false;
		}
		
		return true;
	}
}